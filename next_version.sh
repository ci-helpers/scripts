#!/bin/bash
(
    VERSION=$(git describe --tags --abbrev=0 2>/dev/null)
    echo $VERSION | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}'
)
errorCode=$?

if [ $errorCode -ne 0 ]; then
    echo $DEFAULT_VERSION
fi
