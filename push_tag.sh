#!/bin/bash

if [ "$TAG_SHA" -ne "$CI_COMMIT_SHA" ]; then
    if [ "$TAG" -ne "$DEFAULT_TAG" ]; then
        TAG=$(echo $TAG | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}')
    fi
    git tag "$TAG"
    git push --tags http://root:$BUILD_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:$CI_COMMIT_BRANCH
fi
