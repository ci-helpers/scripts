#!/bin/bash
(
    echo $(git describe --tags --abbrev=0 2>/dev/null)
)
errorCode=$?

if [ $errorCode -ne 0 ]; then
    echo $DEFAULT_VERSION
fi
